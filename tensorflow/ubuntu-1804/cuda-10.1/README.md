This tensorflow package has CUDA support enabled but does NOT require CPU with AVX instruction set.

# Build configuration

* XLA JIT support - Yes
* OpenCL SYCL support - No
* ROCm support - No
* CUDA support - Yes (CUDA 10.1 and cuDNN 7)
* TensorRT support - no
* CUDA capability - 6.1
* AVX - disabled
